
NEST Summer Internship Challenge 2020
=====================================

Requirements
------------

  1. Node >= v12.13.1
  2. MongoDB >= 4.2.7
  3. ReactJS

#### Run the backend
1. Setup the database configuration variables in the ./nestsi-backend/config/db.config.js file with the correct values of the database being used.
```
exports.MONGO_USER = "mongo_user"
exports.MONGO_PASSWORD = "mongo_user_password"
exports.MONGO_HOSTNAME = "hostname"
exports.MONGO_PORT = "port"
exports.MONGO_DB = "db_name"
```

2. Start the server
```
$ cd nestsi-backend
$ npm start
```

#### Run the frontend
1. Setup the server API url in the file ./nestsi-frontend/src/config.js
```
export const API_URL = "http://api_hostname:port"
```
2. Start the frontend app
```
$ cd nestsi-frontend
$ npm start
```


### Install Git
```
# For linux users
$ sudo apt-get install git
```
Checkout this link to install Git on other platforms: [Install Git](https://git-scm.com/book/en/v1/Getting-Started-Installing-Git)


Clone the repository
--------------------

1. Ask one of the mentors to create a new fork of this repository for yourself with the name **nestsi20-[your name]**

2. Clone the repository on your personal computer
```
$ git clone git@gitlab.com:nest-collective/nestsi2020-[your name].git
```


Challenge
---------
Nest Collective is looking to have a directory of _tascas_ (typical Portuguese small restaurant/cafe/bar) which are located near Downtown and Uptown Offices.

Because you're lazy, instead of doing it manually you're going to create a web platform where you're able to add/edit/remove _tascas_ along with some essential data.


### 1. Build a static web page

Here's a simple wireframe of a Web Application where you can just add new entries

![Directory](screenshot.png)


> Start by building a static web page, using only HTML and CSS, mirroring the mockup above. Don't get too much in detail, just keep the same structure.

Fell free to use any CSS frameworks like Bootstrap, Foundation or similar if you are familiar with any. The same applies for any web framework like ReactJS, Angular or VueJS.


### 2. Build a dynamic web application

Now that the main components are in place, continue by making your application dynamic. Try to dinamically serve the _tascas_ displayed on the page from a database.

You are free to choose any kind of technologies you're familiar with to achieve this task, be it **Ruby on Rails**, **Sinatra**, **ExpressJS**, **Django**, or any other!
Also, use whatever database you're used to, be it relational such as **PostgreSQL** and **MySQL**, document based such as **MongoDB** or any other.

Bring in a new feature by adding an _tasca_ to the directory using the form at the end of the page.
> If you're unfamiliar with server-side web frameworks, try to use Javascript to implement the same functionality.

Extra:

Done already? You must be some kind of genius! Here are a couple of small challenges just to keep you occupied a little longer:

- Create validations for the form fields
- Make a search bar at the top of the listing and implementing filtering by name or address.
- Allow the user to Delete an entry by enabling the "Delete" button
- Upload a photo upon creation
- Click on the list to access a _tasca_ details page
- Create the "Edit" screen and implement its functionality
- Add a new feature that you think would be useful
- Show a Map with the location of each _Tasca_ in the details page


Upload your work
----------------

When you're done, be sure to upload your work back to Github by committing directly to master or to any other branch of your choice.

Don't forget to include instructions on how to set up your web application so we can run it locally.


Thank You
----------------

Thanks for spending the day with us and... Godspeed!
