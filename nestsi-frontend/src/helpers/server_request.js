import { API_URL } from '../config';

class ServerRequest{
    
    static getTascas(){
        return fetch(`${API_URL}/tascas/list`).then(res => res.json())
    }
    

    static getTasca(tascaId){
        return fetch(`${API_URL}/tascas/info?tascaId=${tascaId}`).then(res => res.json())
    }

    static updateTasca(tascaId, reqBody){
        return fetch(`${API_URL}/tascas/update?tascaId=${tascaId}`,{
            method: "PUT",
            body: reqBody
        })
    }

    static createTasca(reqBody){
        return fetch(`${API_URL}/tascas/create`,{
            method: "POST",
            // headers: {
            //     "Content-type": "multipart/form-data"
            // },
            body: reqBody
        })
    }

    static deleteTasca(tascaId){
        return fetch(`${API_URL}/tascas/delete?tascaId=${tascaId}`,{
            method: "DELETE"
        })
    }
}

export default ServerRequest