import React, { useState } from 'react';
import {IMG_URL} from '../config'
import {InputField, ImageInputField} from '../components/FormComponents'
import {useTascaData} from '../pages/TascaInfo'
import ServerRequest from '../helpers/server_request'

function UpdateTascaModal(props){
    const {tascaData, setTascaData} = useTascaData(props.tascaId)
    const [newImage, setNewImage] = useState(null)

    function handleFieldOnChange(fieldName, e){
        if(e.target.validity.valid)
            setTascaData({
                ...tascaData,
                [fieldName]: e.target.value
            })
    }

    function submitUpdateTascaRequest(e){
        e.preventDefault()

        let reqBody = new FormData()
        reqBody.append("name", tascaData.name)
        reqBody.append("address", tascaData.address)
        reqBody.append("rating", tascaData.rating)
        if(tascaData.image !== null){
            const splitImg = tascaData.image.split("/")
            reqBody.append("image_name", splitImg[splitImg.length-1])
        }
        
        if(newImage !== null){
            reqBody.append("imageData", newImage)
            reqBody.append("imageType", newImage.type)
        }
        ServerRequest.updateTasca(props.tascaId, reqBody).then(res => props.hideModal(false) )
    }
    
    return (
        <div>
            {Object.keys(tascaData).length > 0 &&
                <form className="tasca-form" onSubmit={submitUpdateTascaRequest}>
                    <h2>Edit tasca</h2>
                    <InputField fieldName="Name" name="name" fieldValue={tascaData.name} setFieldValue={handleFieldOnChange}/>
                    <InputField fieldName="Address" name="address" fieldValue={tascaData.address} setFieldValue={handleFieldOnChange}/>
                    <InputField fieldName="Rating" name="rating" type="number" fieldValue={tascaData.rating} setFieldValue={handleFieldOnChange}/>
                    <ImageInputField fieldName="Image" setFieldValue={e => setNewImage(e.target.files[0])} fieldValue={tascaData.image}/>

                    { newImage === null && tascaData.image !== null ? 
                        <img className="tasca-image" src={IMG_URL+tascaData.image} alt="Unavailable"/>
                    : newImage !== null &&  <img className="tasca-image" src={URL.createObjectURL(newImage)} alt="Unavailable"/> }
                    
                    <div className="actions-bar">
                        <input type="submit" className="btn" value="Edit Tasca" />
                        <button className="btn btn-red" onClick={e => props.hideModal(false)}>Cancel</button>
                    </div>
                </form>}
        </div>
    )
}


export default UpdateTascaModal