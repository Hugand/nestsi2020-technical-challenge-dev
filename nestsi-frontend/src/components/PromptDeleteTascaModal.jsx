import React from 'react';
import ServerRequest from '../helpers/server_request';

function PromptDeleteTascaModal(props){
    return (
        <div>
            <h1>Warning!</h1>
            <h3>Do you really want to delete the tasca with the name <b>{props.tascaName}</b>?</h3>

            <div className="action-buttons">
                <button className="btn btn-red" onClick={() => {
                    ServerRequest.deleteTasca(props.tascaId).then(() => window.location = "/")
                }}>Delete</button>
                <button className="btn btn-secondary" onClick={() => props.hideModal(false)}>Cancel</button>
            </div>
        </div>
    )
}

export default PromptDeleteTascaModal