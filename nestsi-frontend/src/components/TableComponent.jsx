import React, { useState } from 'react';
import '../css/Modal.scss'
import { MdKeyboardArrowLeft, MdKeyboardArrowRight } from 'react-icons/md';

function TableComponent(props){
    const [page, setPage] = useState(1)
   
    const tascas = (props.searchField === "") ? props.tascasList : props.tascasList.filter(tasca => tasca.name.toLowerCase().includes(props.searchField) || tasca.address.toLowerCase().includes(props.searchField))
    const tascasBuff = [...tascas]
    const tascasList = [];
    
    // Split the tascas array for the pagination
    while(tascasBuff.length) {
    	tascasList.push(tascasBuff.splice(0, 7));
    }

    function incPage(){
        if(page < tascasList.length)
            setPage(page+1)
    }
    function decPage(){
        if(page > 1)
            setPage(page-1)
    }

    return(
        <table>
            <thead>
                <tr>
                <th>Name</th>
                <th>Address</th>
                <th>Rating</th>
                <th>Actions</th>
                <th></th>
                </tr>
            </thead>
            <tbody>
                {tascasList.length > 0 ? tascasList[page-1].map(tasca =>
                    <tr key={tasca._id} onClick={() => {window.location = "/tasca?id="+tasca._id}}>
                        <td>{tasca.name}</td>
                        <td>{tasca.address}</td>
                        <td>{tasca.rating}</td>
                        <td><button className="btn btn" onClick={e => {
                            e.stopPropagation()
                            props.toggleUpdateTascaModal(true, tasca._id)
                        }}>Edit</button></td>
                        <td><button className="btn btn-red" onClick={async e => {
                            e.stopPropagation()
                            props.toggleDeleteTascaModal(true, tasca._id, tasca.name)
                        }}>Delete</button></td>
                    </tr>)
                    
                : <tr><td colSpan="5">No tascas available</td></tr>}
            </tbody>

            <tfoot>
                <tr>
                    <td colSpan="5"><button className="btn btn-secondary" onClick={decPage} style={{"marginRight": "50px"}}><MdKeyboardArrowLeft size="20"/></button>
                    <label>{page}</label>
                    <button className="btn btn-secondary" onClick={incPage} style={{"marginLeft": "50px"}}><MdKeyboardArrowRight size="20"/></button></td>
                </tr>
            </tfoot>
        </table>
    )
}

export default TableComponent