import React, { useState } from 'react';
import {InputField, ImageInputField} from '../components/FormComponents'
import ServerRequest from '../helpers/server_request'

function CreateTascaModal(props){
    const [tasca, setTasca] = useState({
        name: "",
        address: "",
        rating: 1,
        image: null
    })

    function handleTascaFieldOnChange(fieldName, e){
        if(e.target.validity.valid)
            setTasca({
                ...tasca,
                [fieldName]: (fieldName === 'image' && e === null) ? null : e.target.value
            })
    }

    function submitCreateTascaRequest(e){
        e.preventDefault()

        let reqBody = new FormData()
        reqBody.append("name", tasca.name)
        reqBody.append("address", tasca.address)
        reqBody.append("rating", tasca.rating)
        if(tasca.image !== null){
            reqBody.append("imageData", tasca.image)
            reqBody.append("imageType", tasca.image.type)
        }

        ServerRequest.createTasca(reqBody)
            .then(() => props.hideModal(false))
    }
    
    return (
        <div>
            <form className="tasca-form" onSubmit={submitCreateTascaRequest}>
                <h2>Create tasca</h2>
                <InputField fieldName="Name" name="name" fieldValue={tasca.name} setFieldValue={handleTascaFieldOnChange}/>
                <InputField fieldName="Address" name="address" fieldValue={tasca.address} setFieldValue={handleTascaFieldOnChange}/>
                <InputField fieldName="Rating" name="rating" type="number" fieldValue={tasca.rating} setFieldValue={handleTascaFieldOnChange}/>
                <ImageInputField fieldName="Image" name="image" fieldValue={tasca.image} setFieldValue={handleTascaFieldOnChange} />

                { tasca.image !== null &&
                    <img className="tasca-image" src={URL.createObjectURL(tasca.image)} alt="Unavailable"/> }
                    
                <div className="actions-bar">
                    <input type="submit" className="btn" value="Add Tasca" />
                    <button className="btn btn-red" onClick={e => props.hideModal(false)}>Cancel</button>
                </div>
            </form>
        </div>
    )
}

export default CreateTascaModal