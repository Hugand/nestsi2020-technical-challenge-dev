import React from 'react';

function ModalBox(props){

    return (
        <div className="modal-bg" onClick={e => {props.hideModal(false)}}>
            <div className="modal-container" onClick={e => e.stopPropagation()}>
                {props.render(props)}
            </div>
        </div>
    )
}

export default ModalBox