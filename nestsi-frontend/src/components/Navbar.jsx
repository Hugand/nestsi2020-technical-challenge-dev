import React from 'react'
import { MdHome } from "react-icons/md";

function Navbar(){
    return (
        <nav>
            <button onClick={e => window.location = "/"}><MdHome color="white" size="28"/></button>
            <h2>Tascas!</h2>
        </nav>
    )
}

export default Navbar