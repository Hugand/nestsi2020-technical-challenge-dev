import React from 'react';

export const ImageInputField = props => {
    return (
        <div className="input-container">
            <label>{props.fieldName}</label>
            <div className="file-field">
                <input type="file" onChange={e => {
                    if(props.name)
                        props.setFieldValue(props.name, e)
                    else
                        props.setFieldValue(e)
                    }} accept="image/png, image/jpeg"/>
            </div>
        </div>
    )
}

export const InputField = props => {
    return (
        <div className="input-container">
            <label>{props.fieldName}</label>
            <input type={(props.type) ? props.type : "text"} className="txt-field" value={props.fieldValue}
                onChange={e => props.setFieldValue(props.name, e)} step={(props.type === "number") ? "0.1" : undefined}
                min={(props.type === "number") ? "1" : undefined} max={(props.type === "number") ? "10" : undefined} />
        </div>
    )
}

export const FieldDisplay = props => {
    return (
        <div className="field-display">
            <label className="label">{props.label}</label>
            <label className="field-value">{props.fieldValue}</label>
        </div>
    )
}