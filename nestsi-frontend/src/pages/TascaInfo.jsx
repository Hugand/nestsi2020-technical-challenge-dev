import React, { useState, useEffect } from 'react';
import '../css/App.scss';
import { useLocation } from 'react-router-dom'
import {FieldDisplay} from '../components/FormComponents'
import ModalBox from '../components/ModalBox'
import UpdateTascaModal from '../components/UpdateTascaModal'
import PromptDeleteTascaModal from '../components/PromptDeleteTascaModal'
import Navbar from '../components/Navbar'
import ServerRequest from '../helpers/server_request';
import {IMG_URL} from '../config'

function TascaInfo(){
    const tascaId = useQuery().get("id")
    const {tascaData, setTascaData} = useTascaData(tascaId)
    const [isUpdateModalDisplayed, setIsUpdateModalDisplayed] = useState(false)
    const [isDeleteModalDisplayed, setIsDeleteModalDisplayed] = useState(false)

    async function updateModalDisplayedState(newIsModalDisplayed){
        setIsUpdateModalDisplayed(newIsModalDisplayed)

        if(!newIsModalDisplayed)
            setTascaData(await ServerRequest.getTasca(tascaId))
    }

    return (
        <div className="App">
             <Navbar />
                
            {isUpdateModalDisplayed &&
                <ModalBox hideModal={updateModalDisplayedState} render={props => <UpdateTascaModal {...props} tascaId={tascaId} />}/>}

            {isDeleteModalDisplayed &&
                <ModalBox hideModal={setIsDeleteModalDisplayed} render={props => <PromptDeleteTascaModal {...props} tascaId={tascaId} tascaName={tascaData.name}/>}/>}

            <div id="container">
                <div className="actions-bar right-aligned-bar">
                    <button className="btn" onClick={async () => setTascaData(await ServerRequest.getTasca(tascaId))}>Refresh</button>
                    <button className="btn" onClick={e => updateModalDisplayedState(true)}>Edit</button>
                    <button className="btn btn-red" onClick={() => setIsDeleteModalDisplayed(true)}>Delete</button>
                </div>
                <div id="field-container">
                    <FieldDisplay label="Name" fieldValue={tascaData.name}/>
                    <FieldDisplay label="Address" fieldValue={tascaData.address}/>
                    <FieldDisplay label="Rating" fieldValue={tascaData.rating}/>
                </div>

                {(tascaData.image !== undefined && tascaData.image !== null) && 
                    <img className="tasca-image" src={IMG_URL+tascaData.image} alt="Unavailable"/>}
            </div>
        </div>
    )
}

export function useTascaData(tascaId){
    const [tascaData, setTascaData] = useState({})

    useEffect(() => {
        async function fetchTasca(){
            setTascaData(await ServerRequest.getTasca(tascaId))   
        }

        fetchTasca()
    }, [tascaId])

    return {
        tascaData: tascaData,
        setTascaData: setTascaData,
    }
}

function useQuery() {
    return new URLSearchParams(useLocation().search);
}


export default TascaInfo