import React, { useState, useEffect } from 'react';
import '../css/App.scss';
import TableComponent from '../components/TableComponent'
import ModalBox from '../components/ModalBox'
import CreateTascaModal from '../components/CreateTascaModal'
import UpdateTascaModal from '../components/UpdateTascaModal'
import Navbar from '../components/Navbar'
import PromptDeleteTascaModal from '../components/PromptDeleteTascaModal'
import { MdSearch } from "react-icons/md";

import ServerRequest from '../helpers/server_request'

function TascasList() {
  const [tascasList, setTascasList] = useState([])
  const [isCreateModalDisplayed, setIsCreateModalDisplayed] = useState(false)
  const [isUpdateModalDisplayed, setIsUpdateModalDisplayed] = useState(false)
  const [editTascaId, setEditTascaId] = useState("")
  const [deleteTascaName, setDeleteTascaName] = useState("")
  const [isDeleteModalDisplayed, setIsDeleteModalDisplayed] = useState(false)
  const [searchField, setSearchField] = useState("")
  

  useEffect(() => {
    async function fetchTascas(){
      setTascasList(await ServerRequest.getTascas())
    }
    fetchTascas()
  }, [])

  async function toggleCreateTascaModal(newIsModalDisplayed){
      setIsCreateModalDisplayed(newIsModalDisplayed)

      if(!newIsModalDisplayed)
        setTascasList(await ServerRequest.getTascas())
  }
  
  async function toggleUpdateTascaModal(newIsModalDisplayed, tascaId = ""){
    setIsUpdateModalDisplayed(newIsModalDisplayed)

    if(!newIsModalDisplayed){
      setTascasList(await ServerRequest.getTascas())
    }

    setEditTascaId(tascaId)
  }

  async function toggleDeleteTascaModal(newIsDeleteModalDisplayed, tascaId = "", tascaName = ""){
    setIsDeleteModalDisplayed(newIsDeleteModalDisplayed)

    if(!newIsDeleteModalDisplayed){
      setTascasList(await ServerRequest.getTascas())
    }

    setEditTascaId(tascaId)
    setDeleteTascaName(tascaName)
  }

  return (
    <div className="App">
      <Navbar />

      <div id="container">
        <h2 style={{"marginBottom": "0"}}>TASCAS!</h2>
        
        {isCreateModalDisplayed &&
          <ModalBox hideModal={toggleCreateTascaModal} render={props => <CreateTascaModal {...props}/>}/>}

        {isUpdateModalDisplayed &&
          <ModalBox hideModal={toggleUpdateTascaModal} render={props => <UpdateTascaModal {...props} tascaId={editTascaId} />}/>}

        {isDeleteModalDisplayed &&
          <ModalBox hideModal={setIsDeleteModalDisplayed} render={props => <PromptDeleteTascaModal {...props} tascaId={editTascaId} tascaName={deleteTascaName}/>}/>}

        <div className="actions-bar">
          <div className="search-field">
            <input type="text" placeholder="search" value={searchField} onChange={e => setSearchField(e.target.value)} />
            <MdSearch size="15" />
          </div>
        </div>

        <TableComponent
          tascasList={tascasList} searchField={searchField}
          refreshTascas={async () => setTascasList(await ServerRequest.getTascas())}
          toggleUpdateTascaModal={toggleUpdateTascaModal} 
          toggleDeleteTascaModal={toggleDeleteTascaModal}/>

        <div className="actions-bar">
          <button className="btn" onClick={async () => setTascasList(await ServerRequest.getTascas())}>Refresh</button>
          <button className="btn" onClick={e => toggleCreateTascaModal(true)}>Create</button>
        </div>
      </div>
    </div>
  );
}

export default TascasList;
