import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TascasList from './pages/TascasList';
import TascaInfo from './pages/TascaInfo';
import { Route, BrowserRouter as Router} from 'react-router-dom'

function routing() {
  return (
      <Router>
          <div>
              <Route exact path="/" component={TascasList}/>
              <Route path="/tasca" component={TascaInfo}/>
          </div>
      </Router>
  )
}

ReactDOM.render(routing(), document.getElementById('root'));