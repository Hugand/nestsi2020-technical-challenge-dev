const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Tasca = new Schema({
	name: {type: String, required: true},
	address: {type: String, required: true},
    rating: {type: Number, required: true},
    image: {type: String}
})

module.exports = mongoose.model('Tasca', Tasca)