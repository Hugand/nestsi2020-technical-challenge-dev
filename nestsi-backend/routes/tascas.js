const express = require('express')
const tascas = require('../controllers/tascas');
const path = require('path')
const image_upload = require('../controllers/image_upload')

module.exports = function(app){
    app.use((req, res, next) => {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    })
    app.use('/tascas_images', express.static(path.join(__dirname, '../tascas_images/')))

    app.get("/tascas/list", (req, res) => tascas.get_tascas_list(req, res))
    app.get("/tascas/info", (req, res) => tascas.get_tasca_info(req, res))
    app.post("/tascas/create", image_upload.upload.single("imageData"), (req, res) => tascas.create_tasca(req, res))
    app.put("/tascas/update", image_upload.upload.single("imageData"), (req, res) => tascas.update_tasca(req, res))
    app.delete("/tascas/delete", (req, res) => tascas.delete_tasca(req, res))
}