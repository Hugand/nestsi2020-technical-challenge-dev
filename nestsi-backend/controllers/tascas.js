const Tasca = require('../models/tasca')
const fs = require('fs');
const path = require('path')

exports.create_tasca = function(req, res){
    const newTasca = req.body
    const areFieldsValid = (Object.values(newTasca).filter(fieldValue => fieldValue === undefined || fieldValue === "" || fieldValue === null).length === 0)
    newTasca.image = null
    if(req.file !== undefined){
        const splitFilePath = req.file.path.split("/")
        const fileName = splitFilePath[splitFilePath.length-1]
        newTasca.image = fileName
    }

    if(newTasca.rating >= 1 && newTasca.rating <= 10 && areFieldsValid){
        Tasca.create(newTasca, (err, tasca) => {
            if(err){ res.status(500).send("Error") }
            res.status(200).send("success")
        })
    }else
        res.status(400).send("invalid data")
}

exports.get_tascas_list = function(req, res){
    Tasca.find({}, "-image", (err, tascas) => {
        if(err){ res.status(500).send("Error") }
        res.status(200).json(tascas)
    })
}

exports.get_tasca_info = function(req, res){
    if(req.query.tascaId !== undefined && req.query.tascaId !== "null")
        Tasca.findById(req.query.tascaId, (err, tascas) => {
            if(err){ res.status(500).send("Error") }
            res.status(200).json(tascas)
        })
    else
        res.status(400).send("Invalid id")
}

exports.update_tasca = function(req, res){
    const updatedTasca = req.body
    const areFieldsValid = (Object.values(updatedTasca).filter(fieldValue => fieldValue === undefined || fieldValue === "" || fieldValue === null).length === 0)

    if(req.query.tascaId !== undefined && areFieldsValid)
        Tasca.findById(req.query.tascaId, async (err, tasca) => {
            if(err){ res.status(500).send("Error") }

            if(updatedTasca.rating >= 1 && updatedTasca.rating <= 10){
                tasca.name = updatedTasca.name
                tasca.address = updatedTasca.address
                tasca.rating = updatedTasca.rating

                if(req.file !== undefined){
                    const splitFilePath = req.file.path.split("/")
                    const fileName = splitFilePath[splitFilePath.length-1]
                    tasca.image = fileName
                }
        
                await tasca.save()

                res.status(200).send("success")
            }else
                res.status(400).send("Invalid data")

        })
    else
        res.status(400).send("Invalid id")
}

exports.delete_tasca = function(req, res){
    if(req.query.tascaId !== undefined){
        Tasca.findById(req.query.tascaId, async (err, tasca) => {
            if(err){ res.status(500).send("Error") }
            deleteExistingTascaImages(tasca.image)
        })

        Tasca.deleteOne({_id: req.query.tascaId}, (err, tascas) => {
            if(err){
                res.status(500).send("Error");
                throw err
            }
            res.status(200).send("success")
        })
    }else
        res.status(400).send("Invalid id")
}

function deleteExistingTascaImages(imageFileNamePrefix){
    const filesList = fs.readdirSync(path.join(__dirname, '../tascas_images/'))

    filesList.forEach(file => {
        if(file.startsWith(imageFileNamePrefix))
            fs.unlinkSync(path.join(__dirname, '../tascas_images/', file))
    });
}
