const multer = require("multer")
const path = require('path')
const fs = require('fs');

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, path.join(__dirname, '../tascas_images/'))
    },
    filename: function(req, file, cb){
        const imageExtension = file.mimetype.split("/")[1]
        const fileName = `tasca-${Date.now()}.${imageExtension}`

        if(req.body.image_name !== undefined)
            deleteExistingTascaImages(req.body.image_name)

        cb(null, fileName)
    }
})

function deleteExistingTascaImages(imageFileNamePrefix){
    const filesList = fs.readdirSync(path.join(__dirname, '../tascas_images/'))

    filesList.forEach(file => {
        if(file.startsWith(imageFileNamePrefix))
            fs.unlinkSync(path.join(__dirname, '../tascas_images/', file))
    });
}

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null, true)
    }else
        cb(null, false)
}

exports.upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024*1024*8
    },
    fileFilter: fileFilter
})