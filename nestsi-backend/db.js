const mongoose = require('mongoose')
const { MONGO_USER,
    MONGO_PASSWORD,
    MONGO_HOSTNAME,
    MONGO_PORT,
    MONGO_DB } = require('./config/db.config')

const url = `mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`

mongoose.connect(url, {userNewUrlParser: true, useUnifiedTopology: true, useNewUrlParser: true})
    .then(() => {
        console.log("CONNECTED TO MONGO")
    })
    .catch(err => console.log("ERROR CONECTING RO MONGO"))