const express = require('express')
const app = express()
const bodyParser = require("body-parser")
const cors = require("cors")

require('./db')

const port = 8080
app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))

require('./routes/tascas')(app)

app.listen(port, function(){
	console.log('Listening on 8080')
})
 